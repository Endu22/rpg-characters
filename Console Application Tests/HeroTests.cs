using Console_Application;
using Console_Application.Attributes;

namespace Console_Application_Tests
{
    public class HeroTests
    {
        #region Creation
        [Fact]
        public void Creation_CreatingWarrior_ShouldBeLevelOne()
        {
            // Arrange
            Hero warrior = new Warrior("Arthur");
            int expected = 1;
            // Act
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Creation_CreatingWarrior_ShouldHaveCorrectAttributesAtLevelOne()
        {
            // Arrange
            Hero warrior = new Warrior("Arthur");
            int strength = 5;
            int dexterity = 2;
            int intelligence = 1;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(warrior.Attributes));
        }

        [Fact]
        public void Creation_CreatingMage_ShouldBeLevelOne()
        {
            // Arrange
            Hero mage = new Mage("Merlin");
            int expected = 1;
            // Act
            int actual = mage.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Creation_CreatingMage_ShouldHaveCorrectAttributesAtLevelOne()
        {
            // Arrange
            Hero mage = new Mage("Arthur");
            int strength = 1;
            int dexterity = 1;
            int intelligence = 8;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(mage.Attributes));
        }

        [Fact]
        public void Creation_CreatingRogue_ShouldBeLevelOne()
        {
            // Arrange
            Hero rogue = new Rogue("Dexter");
            int expected = 1;
            // Act
            int actual = rogue.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Creation_CreatingRogue_ShouldHaveCorrectAttributesAtLevelOne()
        {
            // Arrange
            Hero rogue = new Rogue("Arthur");
            int strength = 2;
            int dexterity = 6;
            int intelligence = 1;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(rogue.Attributes));
        }

        [Fact]
        public void Creation_CreatingRanger_ShouldBeLevelOne()
        {
            // Arrange
            Hero ranger = new Ranger("Jaina");
            int expected = 1;
            // Act
            int actual = ranger.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Creation_CreatingRanger_ShouldHaveCorrectAttributesAtLevelOne()
        {
            // Arrange
            Hero ranger = new Ranger("Arthur");
            int strength = 1;
            int dexterity = 7;
            int intelligence = 1;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(ranger.Attributes));
        }
        #endregion

        #region LevelUp
        [Fact]
        public void Leveling_LevelUpWarrior_ShouldBeLevelTwo()
        {
            // Arrange
            Hero warrior = new Warrior("Arthur");
            int expected = 2;
            warrior.LevelUp();
            // Act
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_LevelUpWarrior_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Hero warrior = new Warrior("Arthur");
            warrior.LevelUp();
            int strength = 8;
            int dexterity = 4;
            int intelligence = 2;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(warrior.Attributes));
        }

        [Fact]
        public void Leveling_LevelUpMage_ShouldBeLevelTwo()
        {
            // Arrange
            Hero mage = new Mage("Arthur");
            int expected = 2;
            mage.LevelUp();
            // Act
            int actual = mage.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_LevelUpMage_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Hero mage = new Mage("Arthur");
            mage.LevelUp();
            int strength = 2;
            int dexterity = 2;
            int intelligence = 13;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(mage.Attributes));
        }

        [Fact]
        public void Leveling_LevelUpRogue_ShouldBeLevelTwo()
        {
            // Arrange
            Hero rogue = new Rogue("Arthur");
            int expected = 2;
            rogue.LevelUp();
            // Act
            int actual = rogue.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_LevelUpRogue_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Hero rogue = new Rogue("Arthur");
            rogue.LevelUp();
            int strength = 3;
            int dexterity = 10;
            int intelligence = 2;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(rogue.Attributes));
        }

        [Fact]
        public void Leveling_LevelUpRanger_ShouldBeLevelTwo()
        {
            // Arrange
            Hero ranger = new Ranger("Arthur");
            int expected = 2;
            ranger.LevelUp();
            // Act
            int actual = ranger.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_LevelUpRanger_ShouldHaveCorrectAttributes()
        {
            // Arrange
            Hero ranger = new Ranger("Arthur");
            ranger.LevelUp();
            int strength = 2;
            int dexterity = 12;
            int intelligence = 2;
            PrimaryAttributes expected = new PrimaryAttributes(strength, dexterity, intelligence);

            // Act & Assert
            Assert.True(expected.IsEqual(ranger.Attributes));
        }
        #endregion
    }
}