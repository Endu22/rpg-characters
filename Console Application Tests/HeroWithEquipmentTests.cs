﻿using Console_Application;
using Console_Application.Attributes;
using Console_Application.enums;
using Console_Application.Exceptions;
using Console_Application.Items.Armor;
using Console_Application.Items.weapons;

namespace Console_Application_Tests
{
    public class HeroWithEquipmentTests
    {





        #region Equip hero with item with too high level
        [Fact]
        public void EquipHero_EquipHeroWithWeaponWhichHaveTooHighLevel_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            // Create test Axe
            string weaponName = "Common axe";
            int itemLevel = 2;
            ItemSlots itemSlot = ItemSlots.WEAPON;
            WeaponTypes weaponType = WeaponTypes.AXE;
            WeaponAttributes weaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 };
            Weapon testAxe = new Weapon(weaponName, itemLevel, itemSlot, weaponType, weaponAttribute);

            // Create warrior
            Hero warrior = new Warrior("Arthur");

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipHero_EquipHeroWithArmorWhichHaveTooHighLevel_ShouldThrowInvalidArmorException()
        {
            // Arrange
            // Create test Armor
            string itemName = "Common plate body armor";
            int itemLevel = 2;
            ItemSlots itemSlot = ItemSlots.BODY;
            ArmorTypes armorType = ArmorTypes.PLATE;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            PrimaryAttributes armorAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
            Armor testPlateBody = new Armor(itemName, itemLevel, itemSlot, armorType, armorAttributes);

            // Create warrior
            Hero warrior = new Warrior("Arthur");

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody));
        }
        #endregion

        #region equip hero with wrong weapon and armor types
        [Fact]
        public void EquipHero_EquipHeroWithWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            // Create test Bow
            string weaponName = "Common bow";
            int itemLevel = 1;
            ItemSlots itemSlot = ItemSlots.WEAPON;
            WeaponTypes weaponType = WeaponTypes.BOW;
            WeaponAttributes weaponAttribute = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 };
            Weapon testBow = new Weapon(weaponName, itemLevel, itemSlot, weaponType, weaponAttribute);

            // Create warrior
            Hero warrior = new Warrior("Arthur");

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipHero_EquipHeroWithWrongArmorType_ShouldThrowInvalidArmorException()
        {
            // Arrange
            // Create test armor
            string itemName = "Common cloth head armor";
            int itemLevel = 1;
            ItemSlots itemSlot = ItemSlots.HEAD;
            ArmorTypes armorType = ArmorTypes.CLOTH;
            int strength = 0;
            int dexterity = 0;
            int intelligence = 5;
            PrimaryAttributes armorAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
            Armor testClothHead = new Armor(itemName, itemLevel, itemSlot, armorType, armorAttributes);


            // Create warrior
            Hero warrior = new Warrior("Arthur");

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothHead));
        }
        #endregion

        #region successful character equips
        [Fact]
        public void successfulEquip_EquipAValidWeaponOnCharacter_ShouldReturnSuccessString()
        {
            // Arrange
            // Create test Axe
            string weaponName = "Common axe";
            int itemLevel = 2;
            ItemSlots itemSlot = ItemSlots.WEAPON;
            WeaponTypes weaponType = WeaponTypes.AXE;
            WeaponAttributes weaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 };
            Weapon testAxe = new Weapon(weaponName, itemLevel, itemSlot, weaponType, weaponAttribute);

            // Create warrior
            Hero warrior = new Warrior("Arthur");
            warrior.LevelUp();

            string expected = "New weapon equipped!";

            // Act
            string actual = warrior.EquipWeapon(testAxe);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void successfulEquip_EquipAValidArmorOnCharacter_ShouldReturnSuccessString()
        {
            // Arrange
            // Create test Armor
            string itemName = "Common plate body armor";
            int itemLevel = 2;
            ItemSlots itemSlot = ItemSlots.BODY;
            ArmorTypes armorType = ArmorTypes.PLATE;
            int strength = 1;
            int dexterity = 0;
            int intelligence = 0;
            PrimaryAttributes armorAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
            Armor testPlateBody = new Armor(itemName, itemLevel, itemSlot, armorType, armorAttributes);

            // Create warrior
            Hero warrior = new Warrior("Arthur");
            warrior.LevelUp();

            string expected = "New armor equipped!";

            // Act
            string actual = warrior.EquipArmor(testPlateBody);

            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region calculate damage
        [Fact]
        public void DamageCaluculation_GetCharacterDamageWhenNoWeaponIsEquipped_ShouldReturnDoubleRepresentingDamage()
        {
            // Arrange
            Hero warrior = new Warrior("Arthur");

            double expected = 1 * (1 + (5 / 100));

            // Act
            double actual = warrior.GetDamage();

            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void DamageCalculation_GetDamageWhithWeaponEquipped_ShouldReturnDoubleRepresentingDamage()
        {
            // Arrange
            // Create test Axe
            string weaponName = "Common axe";
            int itemLevel = 1;
            ItemSlots itemSlot = ItemSlots.WEAPON;
            WeaponTypes weaponType = WeaponTypes.AXE;
            WeaponAttributes weaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 };
            Weapon testAxe = new Weapon(weaponName, itemLevel, itemSlot, weaponType, weaponAttribute);

            // Create warrior
            Hero warrior = new Warrior("Arthur");
            warrior.EquipWeapon(testAxe);

            double expected = (7 * 1.1) * (1 + (5 / 100));

            // Act
            double actual = warrior.GetDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DamageCalculation_GetDamageWhithWeaponAndBodyArmorEquipped_ShouldReturnDoubleRepresentingDamage()
        {
            // Arrange
            // Create test Axe
            string weaponName = "Common axe";
            int itemLevel = 1;
            ItemSlots itemSlot = ItemSlots.WEAPON;
            WeaponTypes weaponType = WeaponTypes.AXE;
            WeaponAttributes weaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 };
            Weapon testAxe = new Weapon(weaponName, itemLevel, itemSlot, weaponType, weaponAttribute);

            // Create test armor
            string itemName = "Common plate body armor";
            ItemSlots armorSlot = ItemSlots.BODY;
            ArmorTypes armorType = ArmorTypes.PLATE;
            int armorStrength = 1;
            int armorDexterity = 0;
            int armorIntelligence = 0;
            PrimaryAttributes armorAttribute = new PrimaryAttributes(armorStrength, armorDexterity, armorIntelligence);
            Armor testBodyPlateArmor = new Armor(itemName, itemLevel, armorSlot, armorType, armorAttribute);

            // Create warrior & equip
            Hero warrior = new Warrior("Arthur");
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testBodyPlateArmor);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = warrior.GetDamage();

            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
