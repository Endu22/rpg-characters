﻿using Console_Application;
using Console_Application.Attributes;
using Console_Application.enums;
using Console_Application.Items.weapons;

namespace Console_Application_Tests
{
    public class WeaponTests
    {
        [Fact]
        public void weaponCreation_CheckWeaponDamage_ShouldReturnTheWeaponDamage()
        {
            // Arrange
            Weapon testAxe = new Weapon("Great Waraxe", 1, ItemSlots.WEAPON, WeaponTypes.AXE, new WeaponAttributes() { Damage = 4, AttackSpeed = 1.1 });
            int expected = 4;
            // Act
            int actual = testAxe.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void weaponCreation_CheckWeaponAttackSpeed_ShouldReturnTheWeaponAttackSpeed()
        {
            // Arrange
            Weapon testAxe = new Weapon("Great Waraxe", 1, ItemSlots.WEAPON, WeaponTypes.AXE, new WeaponAttributes() { Damage = 4, AttackSpeed = 1.1 });
            double expected = 1.1;
            // Act
            double actual = testAxe.AttackSpeed;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void weaponCreation_CheckWeaponDPS_ShouldReturnADoubleRepresentingDPS()
        {
            // Arrange
            Weapon testWand = new Weapon("Luminous wand", 1, ItemSlots.WEAPON, WeaponTypes.WAND, new WeaponAttributes() { Damage = 11, AttackSpeed = 0.9 });
            double expected = 11 * 0.9;
            // Act
            double actual = testWand.getDamagePerSecond();
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
