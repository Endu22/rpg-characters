﻿using Console_Application.Attributes;
using Console_Application.enums;
using Console_Application.Items;
using Console_Application.Items.weapons;

namespace Console_Application
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Hero hero = new Warrior("Arthur");
            hero.LevelUp();
            hero.LevelUp();
            hero.LevelUp();
            Console.WriteLine(hero.ToString());

            Weapon axe = new Weapon("Great War axe", 9, ItemSlots.WEAPON, WeaponTypes.AXE, new WeaponAttributes() { Damage = 4, AttackSpeed = 1.1 });
            hero.EquipWeapon(axe);
        }
    }
}