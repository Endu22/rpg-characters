﻿
using Console_Application.Attributes;
using Console_Application.enums;

namespace Console_Application.Items.Armor
{
    /// <summary>
    /// The <c>Armor</c> class models a piece of armor. The Armor class is also an item.
    /// </summary>
    public class Armor : Item
    {
        // Variables
        private ArmorTypes armorType;
        private PrimaryAttributes armorAttributes;

        /// <summary>
        /// This constructor initializes a new <c>Armor</c> with a <paramref name="name"/>, <paramref name="itemLevel"/>,
        /// <paramref name="itemSlot"/>, <paramref name="armorType"/> and <paramref name="armorAttributes"/>. 
        /// </summary>
        /// <param name="name">the name of the armor piece.</param>
        /// <param name="itemLevel">the level that is required to be able to equip this item.</param>
        /// <param name="itemSlot">the slot in which the item can be equipped.</param>
        /// <param name="armorType">the type of armor.</param>
        /// <param name="armorAttributes">the primary attributes that the item should possess.</param>
        public Armor(string name, int itemLevel, ItemSlots itemSlot, ArmorTypes armorType, PrimaryAttributes armorAttributes) : base(name, itemLevel, itemSlot)
        {
            this.armorType = armorType;
            this.armorAttributes = armorAttributes;
        }

        // Getters & setters

        public ArmorTypes ArmorType
        {
            get { return armorType; }
            set { armorType = value; }
        }

        public PrimaryAttributes ArmorAttributes
        {
            get { return armorAttributes; }
            set { armorAttributes = value; }
        }
    }
}
