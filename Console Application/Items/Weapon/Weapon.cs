﻿using Console_Application.Attributes;
using Console_Application.enums;

namespace Console_Application.Items.weapons
{
    /// <summary>
    /// The <c>Weapon</c> class models a weapon. The weapon class is also an item.
    /// </summary>
    public class Weapon : Item
    {
        // Variables
        private WeaponTypes weaponType;
        private WeaponAttributes weaponAttributes;

        // Constructors

        /// <summary>
        /// This constructor initializes a new <c>Weapon</c> with a <paramref name="name"/>, <paramref name="itemLevel"/>, 
        /// <paramref name="itemSlot"/>, <paramref name="weaponType"/> and <paramref name="weaponAttributes"/>.
        /// </summary>
        /// <param name="name">the name of the weapon.</param>
        /// <param name="itemLevel">the level that is required to be able to equip this item.</param>
        /// <param name="itemSlot">the slot in which the item can be equipped.</param>
        /// <param name="weaponType">the type of weapon</param>
        /// <param name="weaponAttributes">the weapon attributes that the weapon should possess.</param>
        public Weapon(string name, int itemLevel, ItemSlots itemSlot, WeaponTypes weaponType, WeaponAttributes weaponAttributes) : base(name, itemLevel, itemSlot)
        {
            this.weaponType = weaponType;
            this.weaponAttributes = weaponAttributes;
        }

        // Getters & Setters
        public int Damage
        {
            get { return weaponAttributes.Damage; }
        }

        public double AttackSpeed
        {
            get { return weaponAttributes.AttackSpeed; }
        }

        public WeaponTypes WeaponType
        {
            get { return weaponType; }
            set { weaponType = value; }
        }

        public WeaponAttributes WeaponAttributes
        {
            get { return weaponAttributes; }
            set { weaponAttributes = value; }
        }

        // Methods
        public double getDamagePerSecond()
        {
            return (Damage * AttackSpeed);
        }
    }
}
