﻿using Console_Application.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console_Application.Items
{
    /// <summary>
    /// Class <c>Item</c> models an Item.
    /// </summary>
    public abstract class Item
    {
        // Variables
        private string name;
        private int itemLevel;
        private ItemSlots itemSlot;

        // Constructors

        /// <summary>
        /// This constructor is used by classes that implement this abstract class. The <paramref name="name"/>, 
        /// <paramref name="itemLevel"/> and <paramref name="itemSlot"/> is sent through the classes implementing this class.
        /// </summary>
        /// <param name="name">the name of the item.</param>
        /// <param name="itemLevel">the level that is required to be able to equip this item.</param>
        /// <param name="itemSlot">the slot in which the item can be equipped.</param>
        public Item(string name, int itemLevel, ItemSlots itemSlot)
        {
            this.name = name;
            this.itemLevel = itemLevel;
            this.itemSlot = itemSlot;
        }

        // Getters & Setters
        public string Name { get { return name; } set { name = value; } }
        public int ItemLevel { get { return itemLevel; } set { itemLevel = value; } }
        public ItemSlots ItemSlot { get { return itemSlot; } set { itemSlot = value; } }

    }
}
