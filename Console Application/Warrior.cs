﻿using Console_Application.Attributes;
using Console_Application.enums;
using Console_Application.Exceptions;
using Console_Application.Items;
using Console_Application.Items.Armor;
using Console_Application.Items.weapons;

namespace Console_Application
{
    /// <summary>
    /// Class <c>Warrior</c> extends the <c>Hero</c> class.
    /// </summary>
    public class Warrior : Hero
    {
        // Variables
        private readonly ArmorTypes[] armorTypes;
        private readonly WeaponTypes[] weaponTypes;

        // Constructors

        /// <summary>
        /// The <c>Warrior</c> constructor initializes a new Warrior with the following primary attributes:
        /// <list type="bullet">
        /// <item>
        /// <description>Strength == 5.</description>
        /// </item>
        /// <item>
        /// <description>Dexterity == 2.</description>
        /// </item>
        /// <item>
        /// <description>Intelligence == 1.</description>
        /// </item>
        /// </list>
        /// And sets the equippable armor type(s) to be:
        /// <list type="bullet">
        /// <item>
        /// <description>MAIL</description>
        /// </item>
        /// <item>
        /// <description>PLATE</description>
        /// </item>
        /// </list>
        /// It also sets the equippable weapon type(s) to be:
        /// <list type="bullet">
        /// <item>
        /// <description>AXE</description>
        /// </item>
        /// <item>
        /// <description>HAMMER</description>
        /// </item>
        /// <item>
        /// <description>SWORD</description>
        /// </item>
        /// </list>
        /// </summary>
        /// <param name="name">the new Warrior's name.</param>
        public Warrior(string name) : base(name, new PrimaryAttributes(5, 2, 1))
        {
            armorTypes = new ArmorTypes[2] { ArmorTypes.MAIL, ArmorTypes.PLATE };
            weaponTypes = new WeaponTypes[3] { WeaponTypes.AXE, WeaponTypes.HAMMER, WeaponTypes.SWORD };
        }

        // Methods

        /// <summary>
        /// Method <c>LevelUp</c> increments the <example>level</example> of a Warrior and increases the primary attributes by the following:
        /// <list type="bullet">
        /// <item>
        /// <description>Strength by 3.</description>
        /// </item>
        /// <item>
        /// <description>Dexterity by 2.</description>
        /// </item>
        /// <item>
        /// <description>Intelligence by 1.</description>
        /// </item>
        /// </list>
        /// </summary>
        public override void LevelUp()
        {
            level++;
            attributes += new PrimaryAttributes(3, 2, 1);
        }

        /// <summary>
        /// Method <c>EquipArmor</c> checks if the armor given as an argument is valid for a Warrior. 
        /// If valid, the armor is equipped and the total primary attributes is updated.
        /// </summary>
        /// <param name="item">the armor to check and equip</param>
        /// <returns>A string with a message indicating that the armor have sucessfully been equipped.</returns>
        /// <exception cref="InvalidArmorException">
        /// Thrown when the level of the given armor is higher than the level of a Warrior or if the armor type is invalid for a Warrior.
        /// </exception>
        public override string EquipArmor(Armor item)
        {
            if (!(item.ItemLevel <= Level))
                throw new InvalidArmorException("Too low level");

            if (!armorTypes.Contains(item.ArmorType))
                throw new InvalidArmorException("Wrong armor type");

            // Equip armor
            armorSlots[item.ItemSlot] = item;

            UpdateTotalAttributes();

            return "New armor equipped!";
        }

        /// <summary>
        /// Method <c>EquipWeapon</c> checks if the weapon given as an argument is valid for a Warrior. 
        /// If valid, the weapon is equipped and the Warriors's damage is updated.
        /// </summary>
        /// <param name="item">the weapon to check and equip</param>
        /// <returns>A string with a message indicating that the weapon have sucessfully been equipped.</returns>
        /// <exception cref="InvalidWeaponException">
        /// Thrown when the level the given weapon is higher than the level of a Warrior or if the weapon type is invalid for a Warrior.
        /// </exception>
        public override string EquipWeapon(Weapon item)
        {
            if (!(item.ItemLevel <= Level))
                throw new InvalidWeaponException("Too low level");

            if (!weaponTypes.Contains(item.WeaponType))
                throw new InvalidWeaponException("Wrong weapon type");

            // Equip weapon
            armorSlots[item.ItemSlot] = item;

            UpdateHeroDamage();

            return "New weapon equipped!";
        }

        /// <summary>
        /// Method <c>UpdateHeroDamage</c> Finds the weapon slot on a Warrior and calculates the damage based on the 
        /// DPS of the equipped weapon & the amount of Strength a Warrior has.
        /// </summary>
        public override void UpdateHeroDamage()
        {
            foreach (KeyValuePair<ItemSlots, Item?> entry in armorSlots)
            {
                switch (entry.Value)
                {
                    case Weapon weapon:
                        damage = weapon.getDamagePerSecond() * (1 + (totalAttributes.Strength / 100));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
