﻿using Console_Application.Attributes;
using Console_Application.enums;
using Console_Application.Items;
using Console_Application.Items.Armor;
using Console_Application.Items.weapons;
using System.Text;

namespace Console_Application
{
    /// <summary>
    /// Class <c>Hero</c> models a Hero.
    /// </summary>
    public abstract class Hero
    {
        // Variables
        private string name;
        protected int level = 1;
        protected PrimaryAttributes attributes;
        protected PrimaryAttributes totalAttributes;
        protected Dictionary<ItemSlots, Item?> armorSlots;
        protected double damage;

        // Constructors

        /// <summary>
        /// This constructor is used by classes that implement this abstract class. The <paramref name="name"/> and 
        /// <paramref name="atttributes"/> is sent through the classes implementing this class.
        /// </summary>
        /// <param name="name">the name of the hero</param>
        /// <param name="atttributes">the primary attributes that the hero possess.</param>
        public Hero(string name, PrimaryAttributes atttributes)
        {
            this.name = name;
            this.attributes = atttributes;
            armorSlots = new Dictionary<ItemSlots, Item?>()
            {
                { ItemSlots.HEAD, null },
                { ItemSlots.BODY, null },
                { ItemSlots.LEGS, null },
                { ItemSlots.WEAPON, null }
            };
            totalAttributes = new PrimaryAttributes(0, 0, 0);

            this.damage = 1;
        }

        // Getters & Setters
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public PrimaryAttributes Attributes
        {
            get { return attributes; }
        }

        public int Level
        {
            get { return level; }
        }

        public double Damage
        {
            get { return damage; }
        }

        // Methods

        /// <summary>
        /// The <c>UpdateHeroDamage</c> method should update the damage of the hero.
        /// </summary>
        public abstract void UpdateHeroDamage();

        /// <summary>
        /// The <c>LevelUp</c> method should increment the level of a hero.
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// The <c>EquipArmor</c> method should result in hero equipping the <paramref name="item"/> or throw an error.
        /// </summary>
        /// <param name="item">the armor that should be equipped.</param>
        /// <returns>A string with a message indicating that the armor have sucessfully been equipped.</returns>
        public abstract string EquipArmor(Armor item);

        /// <summary>
        /// The <c>EqupWeapon</c> method should result in hero equipping the <paramref name="item"/> or throw an error.
        /// </summary>
        /// <param name="item">the weapon that should be equipped.</param>
        /// <returns>A string with a message indicating that the weapon have sucessfully been equipped.</returns>
        public abstract string EquipWeapon(Weapon item);

        /// <summary>
        /// The <c>UpdateTotalAttributes</c> method iterates through the armor slots and adds all the primary stats
        /// of every armor piece to the total sum of primary stats.
        /// </summary>
        public void UpdateTotalAttributes()
        {
            totalAttributes = new PrimaryAttributes(0, 0, 0); // reset total attribtutes
            foreach (KeyValuePair<ItemSlots, Item?> entry in armorSlots) // add attributes from all equipped armor.
            {
                switch (entry.Value)
                {
                    case Armor armor:
                        totalAttributes += armor.ArmorAttributes;
                        continue;
                    default:
                        break;
                }
            }
            totalAttributes += attributes; // Add attributes from base atrributes.
        }

        /// <summary>
        /// The <c>GetDamage</c> method returns the damage of the hero.
        /// </summary>
        /// <returns>A double representing the damage that the hero can deal.</returns>
        public double GetDamage()
        {
            return Damage;
        }

        /// <summary>
        /// The <c>DisplayCharacterStats</c> use the overriden <c>ToString</c> method to get information about the hero's name, level, stats and damage.
        /// </summary>
        /// <returns>A string containing information about the hero's name, level, stats and damage.</returns>
        public string DisplayCharacterStats()
        {
            return ToString();
        }

        /// <summary>
        /// The <c>ToString</c> method constructs a string containing information about the hero's name, level, stats and damage.
        /// </summary>
        /// <returns>A string containing information about the hero's name, level, stats and damage.</returns>
        public override string ToString()
        {
            StringBuilder buf = new StringBuilder();
            buf.Append("Name: ").Append(Name).Append('\n');
            buf.Append("Level: ").Append(Level).Append('\n');
            buf.Append("Strength: ").Append(totalAttributes.Strength).Append('\n');
            buf.Append("Dexterity: ").Append(totalAttributes.Dexterity).Append('\n');
            buf.Append("Intelligence: ").Append(totalAttributes.Intelligence).Append('\n');
            buf.Append("Damage: ").Append(Damage);
            return buf.ToString();
        }
    }
}
