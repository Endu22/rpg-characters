﻿namespace Console_Application.Attributes
{
    /// <summary>
    /// Class <c>PrimaryAttributes</c> models attributes for a hero or armor. 
    /// </summary>
    public class PrimaryAttributes
    {
        private int strength;
        private int dexterity;
        private int intelligence;

        /// <summary>
        /// Initializes a new instance of the <c>PrimaryAttributes</c> class with the values of <paramref name="strength"/>, <paramref name="dexterity"/> and <paramref name="intelligence"/>.
        /// </summary>
        /// <param name="strength">an integer represening the stat strength.</param>
        /// <param name="dexterity">an integer representing the stat dexterity.</param>
        /// <param name="intelligence">an integer representing the stat intelligence.</param>
        public PrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }

        public int Strength
        {
            get { return strength; }
            set { strength = value; }
        }

        public int Dexterity
        {
            get { return dexterity; }
            set { dexterity = value; }
        }

        public int Intelligence
        {
            get { return intelligence; }
            set { intelligence = value; }
        }

        /// <summary>
        /// Takes two <c>PrimaryAttributes</c> classes and sums all of the variables.
        /// </summary>
        /// <param name="left">the left PrimaryAttributes.</param>
        /// <param name="right">the right PrimaryAttributes.</param>
        /// <returns>A new <c>PrimaryAttributes</c> with the summed values of <paramref name="left"/> and <paramref name="right"/>.</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes left, PrimaryAttributes right)
            => new PrimaryAttributes(left.strength + right.strength, left.dexterity +
                right.dexterity, left.intelligence + right.intelligence);

        /// <summary>
        /// The <c>IsEqual</c> method checks if the left instance of PrimaryAttributes has the same values as the <paramref name="other"/> instance.
        /// </summary>
        /// <param name="other">the Primary attributes which the left instance should be compared to.</param>
        /// <returns>A boolean that is <c>True</c> if all values of both instances are equal. Otherwise <c>False</c>.</returns>
        public bool IsEqual(PrimaryAttributes other)
        {
            if (strength == other.Strength && dexterity == other.Dexterity && intelligence == other.Intelligence)
                return true;

            return false;
        }
    }
}
