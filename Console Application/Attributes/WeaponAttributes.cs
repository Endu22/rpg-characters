﻿namespace Console_Application.Attributes
{
    /// <summary>
    /// Class <c>WeaponAttributes</c> models attributes for a weapon. 
    /// </summary>
    public class WeaponAttributes
    {
        public int Damage { get; set; }

        public double AttackSpeed { get; set; }
    }
}
