﻿using Console_Application.Attributes;
using Console_Application.enums;
using Console_Application.Exceptions;
using Console_Application.Items;
using Console_Application.Items.Armor;
using Console_Application.Items.weapons;

namespace Console_Application
{
    /// <summary>
    /// Class <c>Mage</c> extends the <c>Hero</c> class.
    /// </summary>
    public class Mage : Hero
    {
        // Variables
        private readonly ArmorTypes[] armorTypes;
        private readonly WeaponTypes[] weaponTypes;

        // Constructors

        /// <summary>
        /// The <c>Mage</c> constructor initializes a new Mage with the following primary attributes:
        /// <list type="bullet">
        /// <item>
        /// <description>Strength == 1.</description>
        /// </item>
        /// <item>
        /// <description>Dexterity == 1.</description>
        /// </item>
        /// <item>
        /// <description>Intelligence == 8.</description>
        /// </item>
        /// </list>
        /// And sets the equippable armor type(s) to be:
        /// <list type="bullet">
        /// <item>
        /// <description>CLOTH</description>
        /// </item>
        /// </list>
        /// It also sets the equippable weapon type(s) to be:
        /// <list type="bullet">
        /// <item>
        /// <description>STAFF</description>
        /// </item>
        /// <item>
        /// <description>WAND</description>
        /// </item>
        /// </list>
        /// </summary>
        /// <param name="name">the new Mage's name.</param>
        public Mage(string name) : base(name, new PrimaryAttributes(1, 1, 8))
        {
            armorTypes = new ArmorTypes[1] { ArmorTypes.CLOTH };
            weaponTypes = new WeaponTypes[2] { WeaponTypes.STAFF, WeaponTypes.WAND };
        }

        // Methods

        /// <summary>
        /// Method <c>LevelUp</c> increments the <example>level</example> of a Mage and increases the primary attributes by the following:
        /// <list type="bullet">
        /// <item>
        /// <description>Strength by 1.</description>
        /// </item>
        /// <item>
        /// <description>Dexterity by 1.</description>
        /// </item>
        /// <item>
        /// <description>Intelligence by 5.</description>
        /// </item>
        /// </list>
        /// </summary>
        public override void LevelUp()
        {
            level++;
            attributes += new PrimaryAttributes(1, 1, 5);
        }

        /// <summary>
        /// Method <c>EquipArmor</c> checks if the armor given as an argument is valid for a Mage. 
        /// If valid, the armor is equipped and the total primary attributes is updated. 
        /// </summary>
        /// <param name="item">the armor to check and equip.</param>
        /// <returns>A string with a message indicating that the armor have sucessfully been equipped.</returns>
        /// <exception cref="InvalidArmorException">
        /// Thrown when the level of the given armor is higher than the level of a Mage or if the armor type is invalid for a Mage.
        /// </exception>
        public override string EquipArmor(Armor item)
        {
            if (!(item.ItemLevel <= Level))
                throw new InvalidArmorException("Too low level");

            if (!armorTypes.Contains(item.ArmorType))
                throw new InvalidArmorException("Wrong armor type");

            // Equip armor
            armorSlots[item.ItemSlot] = item;

            UpdateTotalAttributes();

            return "New armor equipped!";
        }

        /// <summary>
        /// Method <c>EquipWeapon</c> checks if the weapon given as an argument is valid for a Mage. 
        /// If valid, the weapon is equipped and the Mage's damage is updated.
        /// </summary>
        /// <param name="item">the weapon to check and equip.</param>
        /// <returns>A string with a message indicating that the weapon have sucessfully been equipped.</returns>
        /// <exception cref="InvalidWeaponException">
        /// Thrown when the level the given weapon is higher than the level of a Mage or if the weapon type is invalid for a Mage.
        /// </exception>
        public override string EquipWeapon(Weapon item)
        {
            if (!(item.ItemLevel <= Level))
                throw new InvalidWeaponException("Too low level");

            if (!weaponTypes.Contains(item.WeaponType))
                throw new InvalidWeaponException("Wrong weapon type");

            // Equip weapon
            armorSlots[item.ItemSlot] = item;

            UpdateHeroDamage();

            return "New weapon equipped!";
        }

        /// <summary>
        /// Method <c>UpdateHeroDamage</c> Finds the weapon slot on a Mage and calculates the damage based on the 
        /// DPS of the equipped weapon & the amount of Intelligence a Mage has.
        /// </summary>
        public override void UpdateHeroDamage()
        {
            foreach (KeyValuePair<ItemSlots, Item?> entry in armorSlots)
            {
                switch (entry.Value)
                {
                    case Weapon weapon:
                        damage = weapon.getDamagePerSecond() * (1 + (totalAttributes.Intelligence / 100));
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
