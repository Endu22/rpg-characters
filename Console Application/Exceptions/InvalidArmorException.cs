﻿
namespace Console_Application.Exceptions
{
    /// <summary>
    /// Class <c>InvalidArmorException</c> models a custom exception. Hence, it extends the <c>Exception</c> class.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// The <c>InvalidArmorException</c> constructor initializes a new InvalidArmorException which extends the <c>Exception</c> class.
        /// </summary>
        public InvalidArmorException()
        {
        }

        public InvalidArmorException(string message) : base(message)
        {
        }
    }
}
