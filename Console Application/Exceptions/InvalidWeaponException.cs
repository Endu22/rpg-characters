﻿
namespace Console_Application.Exceptions
{
    /// <summary>
    /// Class <c>InvalidWeaponException</c> models a custom exception. Hence, it extends the <c>Exception</c> class.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        /// <summary>
        /// The <c>InvalidWeaponException</c> constructor initializes a new InvalidWeaponException which extends the <c>Exception</c> class.
        /// </summary>
        public InvalidWeaponException()
        {
        }

        public InvalidWeaponException(string message) : base(message)
        {
        }
    }
}
