﻿
namespace Console_Application.enums
{
    /// <summary>
    /// Enum <c>ArmorTypes</c> models the different types of armor that can exists.
    /// </summary>
    public enum ArmorTypes
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
}
