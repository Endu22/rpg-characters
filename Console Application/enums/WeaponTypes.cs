﻿
namespace Console_Application.enums
{
    /// <summary>
    /// Enum <c>WeaponTypes</c> models the different types of weapons that can exists.
    /// </summary>
    public enum WeaponTypes
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }
}
