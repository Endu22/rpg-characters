﻿
namespace Console_Application.enums
{
    /// <summary>
    /// Enum <c>ItemSlots</c> models all the slots which a hero can equip items in.
    /// </summary>
    public enum ItemSlots
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
