# RPG Characters

### Contributors
Oliver Rimmi - oliver.rimmi@se.experis.com

### Acknowledgments
This project was created as a solution during the Experis Bootcamp (2022) with Noroff Education AS.

### Description
The solution allows for creation of characters for a RPG game. The characters can equip items and weapons with different stats. The characters can be of different classes and have different stats as well as different stat values. The solution also includes a XUnit project which was used to test the solution. 

### Dependencies
Visual Studio Community 22 (.NET 6)
& Windows 10

### Install
Clone repository and open with visual studio.
